<?php 
	include("Base.php");
	require_once("includes/db_connection.php");
	
if(!isset($_SESSION["id"]))
	redirect_to("index.php");
$id=$_SESSION["id"];
$account= find_contestant_by_id($id);
if (!$account)
{
	redirect_to("index.php");
}

//statistics query
$query = "SELECT name,contest_id, SUM(score) ";
$query.= "FROM contestant_joins AS cj , contest AS c ";
$query.= "WHERE c.id=cj.contest_id AND cj.contestant_id={$id} ";
$query.= "GROUP BY contest_id;";

$result=mysqli_query($connection,$query);
confirm_query($result);

$contests_names=array();					//array to store names of all contests the contestant participated in
$contestant_score=array();				    //~~~~~~~~~~~~~~ scores ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$size=0;
while ($row=mysqli_fetch_row($result))
{
	$contests_names[]=$row[0];
	$contestant_score[]=$row[2];
	$size++;							   //storing size of the result to Output them
}
?>
<style type="text/css">
.userImage {
	float: right;
	clear: none;
	width: 150px;
	height: 200px;
	border: 1px solid black;
}
</style>
<div id="rightPan">
	<h1>Profile</h1>
	<h2><?php echo "<td>{$account["handle"]} </td>" ?></h2>
	<h3>Solved Problems</h3>
	<hr />
	<h3>Stats</h3>
	<hr />
</div>

<img src="whatever.jpg" class="userImage" />

<div id="chart_div" style="float: left; margin-top:50px;"></div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>    
<script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.

      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

       // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Contest');
      data.addColumn('number', 'Score');      
      var data_size				=<?php echo json_encode($size); ?>;
      var data_contests_names	=<?php echo json_encode(array_values($contests_names)); ?>;
      var data_contestant_scores=<?php echo json_encode(array_values($contestant_score)); ?>;  
      document.getElementById("chart_div").innerHTML=data_size;
  	 var i=0;
     for (;i<data_size;i++){     
      	var data_array=[data_contests_names[i],Number(data_contestant_scores[i])];
      	data.addRows([data_array]);      	
      }

      // Set chart options
      var options = {'title':'Contestant\'s Statistics' ,
                     'width':700,
                     'height':300
                 };

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }       
    </script>

<?php 
include("Footer.php");
?>