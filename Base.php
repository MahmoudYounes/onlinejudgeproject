<?php
	require_once("includes/functions.php");
	require_once("includes/session.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OnlineJudge</title>
		<link href="includes/prettify/src/prettify.css" type="text/css" rel="stylesheet" />
		<script src="includes/prettify/src/prettify.js" type="text/javascript"></script>
		
		<style type="text/css">
		body {
			font-family: Georgia;
			width: 		 1000px;
			margin:	 	 0 auto;
			font-size: 	 12px;
		}
		h1 {
			font-size: 		 37px;
			margin-top: 	 5px;
			margin-left: 	 15px;
			text-decoration: underline;
		}
		.itemDiv
		{
			width: 			  70%;
			min-width: 		  700px;
			clear: 			  none;
			margin-top: 	  5px;
			height: 		  auto;
			padding: 		  10px;
			background-color: #ddd;
			border-radius: 	  5px;
			border: 		  1px solid black;
		}
		.divName{
			margin-bottom:  30px;
			clear: 		 	none;
			font-family: 	Helvetica;
			font-size: 	 	20px;
			font-weight: 	bold;
			text-shadow: 	1px 1px 1px #ccc;
			
		}
		.announce {
			width: 			700px;
			padding:		2px;
			margin: 		0px;
			clear: 		 	none;
			font-family: 	Helvetica;
			font-size: 	 	14px;
			font-weight: 	bold;
			text-shadow: 	1px 1px 1px #ccc;
		}
		.divTopBar {
			float: right;
			clear: none;
		}
		.login {
			margin: 			100px;
			border-style:  		double;
			border-width: 		2px;
			padding: 			50px;
			border-color: 		#993333;	
			background-color: 	#C0C0C0;
		}
		.createContest {
			border-style: groove;
			width: 770px;
			height: 500px;
			padding-top: 20px;
			border-width: 2	px;

			float: right;
		}
		#header {
			margin:	 		0 auto;
			width: 			40%;
			font-size: 		50px;
			font-weight:	bolder;
			font-family:	Verdana;
			padding: 		10px;
		}
		#header a {
			color: black;
			text-decoration: none;
		}
		#header a:visited{
			color: black;
		}
		#header a:hover{
			color: black;
		}
		#topBar {
			width: 			  98%;
			height: 		  18px;
			padding: 		  8px;
			padding-left: 	  2%;
			padding-right: 	  0;
			font-weight: 	  bold;
			background-color: #855;
			color: 			  #eee;
			text-shadow: 	  1px 1px 1px #111;
			
		}
		#topBar a {
			color: #eee;
			text-decoration: none;
		} 
		#topBar a:visited {
			color: #eee;
		} 
		#topBar a:hover {
			color: #eee;
		} 
		#leftPan {
			float: 			  left;
			clear: 			  none;
			width: 			  20%;
			min-width: 		  200px;
			height: 	  	  500px;
			border: 		  1px solid black;
			border-radius: 	  5px;
			background-color: #eee;
		}
		#leftPan li {
			margin: 		10px;
			margin-bottom: 	15px;
			font-family: 	Helvetica;
			font-weight: 	bold;
			cursor: 		pointer;
		}
		#rightPan {
			float: 			left;
			clear: 			none;
			margin-left: 	20px;
		}
		#footer {
			width: 			150px;
			margin: 		20px auto;
			clear: 			both;
			padding-top: 	1em;
		}
		div.message{
			border: 	 2px solid #8D0D19; 
			color: 		 #8D0D19;
			font-weight: bold;
			margin: 	 2em auto;
			padding: 	 1em;
		}
		div.error{
			border: 	 2px solid #FF0D19; 
			color: 		 #FF0D19; 
			font-weight: bold;
			margin: 	 2em auto; 
			padding: 	 1em ;
		}
		div.error ul{
			margin: 	 0; 
			padding-top: 1em;
		}
		</style>
	</head>
	<body onload="prettyPrint()">
		<div id="header">
			<a href="index.php">OnlineJudge</a>
		</div>
		<hr/>
		<div id="topBar">
			<?php
				if(!logged_in())
				{
					echo "<a href=\"Login.php\">Login</a> 	   &nbsp;&nbsp;|&nbsp;&nbsp;";
					echo "<a href=\"Register.php\">Sign up</a> &nbsp;&nbsp;|&nbsp;&nbsp;";
				}
				else
				{
					echo "<a href=\"Logout.php\">Logout</a>   &nbsp;&nbsp;|&nbsp;&nbsp;";
					echo "<a href=\"Profile.php\">Profile</a> &nbsp;&nbsp;|&nbsp;&nbsp;";
				}
				
			?>
			About &nbsp;&nbsp;|&nbsp;&nbsp; Contact Us
		</div>
		<hr/>
		<div id="leftPan">
			<ul>
			<li>Profile</li>
			<li>My Team</li>
			<li>My Submissions</li>
			<li>Top Coders</li>
			</ul>
		</div>