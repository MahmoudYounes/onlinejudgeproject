<style type="text/css">
	
	.problem
	{
		width: 			  70%;
		min-width: 		  700px;
		min-height: 	  300px;
		clear: 			  none;
		margin-top: 	  5px;
		height: 		  auto;
		padding: 		  10px;
		border-radius: 	  5px;
		border: 		  1px solid black;
	}

</style>

<?php

include("Base.php");
require_once("includes/db_connection.php");

if (!isset($_GET["problem"]))
	redirect_to("index.php");


$query  = "SELECT * ";
$query .= "FROM tutorial ";
$query .= "WHERE id={$_GET["problem"]}";


$results=mysqli_query($connection,$query);
confirm_query($results);

$tutorials=query_result_to_array($results);
$message="";
if(isset($_GET["submit"]))
{
	if (isset($_GET["start2"]) && isset($_GET["tutorialID"]) && isset($_GET["contestantID"]))
		{
			$rating=$_GET["start2"];
			$tutorialID=$_GET["tutorialID"];
			$contestantID=$_GET["contestantID"];
			//inserting rating 

			$query 	= "INSERT INTO tutorial_rating (";
			$query .= "tutorial_id,account_id,rating) VALUES (";
			$query .= "{$tutorialID},{$contestantID},{$rating}) ";

			$results=mysqli_query($connection,$query);
			if (!$results)
			{
				$message="already rated this tutorial";
			}
		}		

}

if (! empty($tutorials))
		$i=1;
		foreach($tutorials as $element)
		{
			$account_handle=find_account_by_id($element["account_id"]);			
			$tutorial_rating=compute_tutorial_rating($element["id"]);		
			echo "						
				<div class=\"itemDiv\" style=\"margin-left:100px; padding-left:200px\">
					
					{$message}<br/>

					<span class=\"divName\">
						tutorial {$i}<br/>
						current rating: {$tutorial_rating["rating"]}
					</span>										
					<div>
					<h2>Description</h2>
					{$element["text"]}
					<br/><br/><br/>
					<span>
					tutorial by: {$account_handle["handle"]}
					</span>
					<br/><br/><br/>
					<form method=\"GET\" action=\"view_tutorial.php\">
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"1\"/> 
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"2\"/>
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"3\"/>
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"4\"/>
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"5\"/>						
						<input name=\"problem\" type=\"hidden\" value=\"{$_GET["problem"]}\">
						<input name=\"tutorialID\" type=\"hidden\" value=\"{$element["id"]}\" />
						<input name=\"contestantID\" type=\"hidden\" value=\"{$_SESSION["id"]}\" />
					<button type=\"submit\" name=\"submit\" value=\"submit\">Submit</button>
					</form>
					</div>
				</div>				
				";
				$i++;
		}

?>		

<script type="text/javascript" src="jQuery/StarRating/jquery.js"></script>
<script src='jQuery/StarRating/jquery.js' type="text/javascript"></script>
<script src='jQuery/StarRating/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
<script src='jQuery/StarRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
<link href='jQuery/StarRating/jquery.rating.css' type="text/css" rel="stylesheet"/>