<?php 
	include("Base.php");	
	require_once("includes/db_connection.php");
	$message="";
	if(isset($_GET["submit"]))
	{
		if (isset($_GET["start2"]) && isset($_GET["contestID"]) && isset($_GET["contestantID"]))
		{
			$rating=$_GET["start2"];
			$contestID=$_GET["contestID"];
			$contestantID=$_GET["contestantID"];
			//inserting rating 

			$query 	= "INSERT INTO contest_rating (";
			$query .= "contest_id,account_id,rating) VALUES (";
			$query .= "{$contestID},{$contestantID},{$rating}) ";

			$results=mysqli_query($connection,$query);
			if (!$results)
			{
				$message="already rated the contest";
			}
		}		
	}
?>
<div id="rightPan">
	<h1>Contests</h1>
	<h2>Recent Contests</h2>
<?php 
	global $connection;

	$query  = "SELECT * ";
	$query .= "FROM Contest ";
	$query .= "ORDER BY start_time ASC LIMIT 10";
	
	$result = mysqli_query($connection, $query);
	confirm_query($result);
	$result_arr = query_result_to_array($result);
	//print_r($result_arr);
	if (! empty($result_arr))
		foreach($result_arr as $contest)
		{
			if ($contest["approved"] == "Approved")
			{
				$c_type = $contest["type"] == 0 ? "Individual" : "Team";
				$contest_rating=compute_contest_rating($contest["id"]);
				echo "
				
				<div class=\"itemDiv\">
					<span class=\"divName\">
						<a href=\"ContestProblems.php?contest={$contest["id"]}\" style=\"text-decoration:none; color:inherit;\">
							{$contest["name"]}
						</a>
					</span>					
					{$message}
					<div class=\"divTopBar\">
					{$contest["start_time"]} | {$contest["end_time"]} | {$c_type} | Current Rating: {$contest_rating["rating"]}<br/>					
					<form method=\"GET\" action=\"Contests.php?\">
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"1\"/> 
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"2\"/>
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"3\"/>
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"4\"/>
						<input name=\"start2\" type=\"radio\" class=\"star\" value=\"5\"/>
						<input name=\"contestID\" type=\"hidden\" value=\"{$contest["id"]}\" />
						<input name=\"contestantID\" type=\"hidden\" value=\"{$_SESSION["id"]}\" />
					<button type=\"submit\" name=\"submit\" value=\"submit\">Submit</button>
					</form>
					<br/><br/><br/>
					
					</div>
				</div>
				
				";
			}
		/*
			echo "
			<div class=\"itemDiv\">
				<span class=\"divName\">
					Awesome itemDiv
				</span>
				<div class=\"divTopBar\">
				StartDate | EndDate | #Problems | individual/team | Registerers
				</div>
			</div>
			";*/
		}

?>
</div>
<script type="text/javascript" src="jQuery/StarRating/jquery.js"></script>
<script src='jQuery/StarRating/jquery.js' type="text/javascript"></script>
<script src='jQuery/StarRating/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
<script src='jQuery/StarRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
<link href='jQuery/StarRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
<?php 
include("Footer.php");
?>